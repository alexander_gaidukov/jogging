//
//  Constants.swift
//  Jogging
//
//  Created by Alexandr Gaidukov on 06/04/2017.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import Foundation

struct Constants {
    struct Server {
        static let scheme = "https"
        static let host = "joggingtrack.herokuapp.com"
    }
}
