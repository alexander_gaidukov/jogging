//
//  PopoverMessageViewController.swift
//  Jogging
//
//  Created by Alexandr Gaidukov on 03/04/2017.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import UIKit
import PureLayout

class PopoverMessageViewController: UIViewController {
    
    private let message: String
    
    private lazy var label: UILabel = {
        let label = UILabel(frame: .zero)
        label.backgroundColor = .clear
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        label.numberOfLines = 0
        label.font = self.font
        return label
    }()
    
    private let font = UIFont.systemFont(ofSize: 14.0)
    
    private let labelInsets = UIEdgeInsets(top: 10.0, left: 15.0, bottom: 10.0, right: 15.0)
    
    init(message: String) {
        self.message = message
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.autoPinEdgesToSuperviewEdges(with: labelInsets)
        label.text = message
    }
    
    func size(forWidth width: CGFloat) -> CGSize {
        let labelWidth = width - labelInsets.left - labelInsets.right
        let labelHeight = (message as NSString).boundingRect(with: CGSize(width: labelWidth, height: .greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil).height
        
        return CGSize(width: width, height: labelHeight + labelInsets.top + labelInsets.bottom)
    }
}
