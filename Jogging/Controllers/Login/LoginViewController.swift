//
//  LoginViewController.swift
//  Jogging
//
//  Created by Alexander Gaidukov on 3/29/17.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LoginViewController: UIViewController, ErrorHandler {
    
    @IBOutlet private weak var containerHeight: NSLayoutConstraint!
    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var logoView: UIImageView!
    @IBOutlet private weak var logoViewCenterConstraint: NSLayoutConstraint!
    
    @IBOutlet private weak var emailTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var loginButton: UIButton!
    @IBOutlet private weak var signUpButton: UIButton!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    private let minimumContainerHeight: CGFloat = 395.0
    private let stackViewHeight: CGFloat = 75.0
    
    private var needToShowPage: Bool {
        return scrollView.isHidden
    }
    
    private let disposeBag = DisposeBag()
    private lazy var loginViewModel: LoginViewModel = {
        
        return LoginViewModel(email: self.emailTextField.rx.text.orEmpty.asDriver(), password: self.passwordTextField.rx.text.orEmpty.asDriver(), loginDidTap: self.loginButton.rx.tap.asDriver(), signUpDidTap: self.signUpButton.rx.tap.asDriver())
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        containerHeight.constant = max(minimumContainerHeight, view.bounds.height)
        configureUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if needToShowPage {
            showPage()
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        containerHeight.constant = max(minimumContainerHeight, size.height)
    }
    
    private func configureUI() {
        
        loginViewModel.isLoginButtonEnabled.drive(loginButton.rx.isEnabled).addDisposableTo(disposeBag)
        loginViewModel.loginDidTap.do(onNext: {[unowned self] in self.view.endEditing(true)}).map { true }.drive(activityIndicator.rx.isAnimating).addDisposableTo(disposeBag)
        loginViewModel.didComplete.map { _ in false }.drive(activityIndicator.rx.isAnimating).addDisposableTo(disposeBag)
        
        loginViewModel.didComplete.drive(onNext: {[weak self] error in
            if let error = error {
                self?.handle(error: error)
            }
        }).addDisposableTo(disposeBag)
    }
    
    private func showPage(animated: Bool = true) {
        
        if animated {
            
            performLaunchAnimation()
            
        } else {
            
            logoView.isHidden = true
            scrollView.isHidden = false
        }
       
    }
    
    private func performLaunchAnimation() {
        let constraintValue = (containerHeight.constant - stackViewHeight) / 4.0 + stackViewHeight / 2.0
        
        let parameters = UICubicTimingParameters(animationCurve: .easeInOut)
        let animator = UIViewPropertyAnimator(duration: 0.5, timingParameters: parameters)
        
        animator.addAnimations {
            self.view.layoutIfNeeded()
        }
        
        animator.addCompletion { _ in
            self.scrollView.alpha = 0.0
            self.scrollView.isHidden = false
            
            let alphaAnimator = UIViewPropertyAnimator(duration: 0.5, timingParameters: parameters)
            
            alphaAnimator.addAnimations {
                self.scrollView.alpha = 1.0
            }
            
            alphaAnimator.addCompletion {_ in
                self.logoView.isHidden = true
            }
            
            alphaAnimator.startAnimation()
        }
        
        self.logoViewCenterConstraint.constant = -constraintValue
        
        animator.startAnimation()
    }
}
