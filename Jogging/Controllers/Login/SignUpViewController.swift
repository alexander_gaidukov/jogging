//
//  SignUpViewController.swift
//  Jogging
//
//  Created by Alexander Gaidukov on 3/31/17.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SignUpViewController: UIViewController, ErrorHandler {
    
    @IBOutlet private weak var containerHeight: NSLayoutConstraint!
    
    @IBOutlet private weak var emailTextField: UITextField!
    @IBOutlet private weak var nameTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var passwordConfirmationTextField: UITextField!
    
    @IBOutlet private weak var passwordInfoButton: UIButton!
    
    @IBOutlet private weak var signInButton: UIButton!
    @IBOutlet private weak var signUpButton: UIButton!
    
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    private let minimumContainerHeight: CGFloat = 485.0
    
    private let disposeBag = DisposeBag()
    
    private lazy var signUpViewModel: SignUpViewModel = {
        return SignUpViewModel(email: self.emailTextField.rx.text.orEmpty.asDriver(), name: self.nameTextField.rx.text.orEmpty.asDriver(), password: self.passwordTextField.rx.text.orEmpty.asDriver(), passwordConfirmation: self.passwordConfirmationTextField.rx.text.orEmpty.asDriver(), signInTap: self.signInButton.rx.tap.asDriver(), signUpTap: self.signUpButton.rx.tap.asDriver())
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        containerHeight.constant = max(minimumContainerHeight, view.bounds.height)
        configureUI()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        containerHeight.constant = max(minimumContainerHeight, size.height)
    }
    
    private func configureUI() {
        signUpViewModel.emailBorderColor.drive(emailTextField.rx.borderColor).addDisposableTo(disposeBag)
        signUpViewModel.nameBorderColor.drive(nameTextField.rx.borderColor).addDisposableTo(disposeBag)
        signUpViewModel.passwordBorderColor.drive(passwordTextField.rx.borderColor).addDisposableTo(disposeBag)
        signUpViewModel.passwordConfirmationBorderColor.drive(passwordConfirmationTextField.rx.borderColor).addDisposableTo(disposeBag)
        
        signUpViewModel.signUpButtonEnable.drive(signUpButton.rx.isEnabled).addDisposableTo(disposeBag)
        signUpViewModel.passwordInfoButtonHidden.drive(passwordInfoButton.rx.isHidden).addDisposableTo(disposeBag)
        
        passwordInfoButton.rx.tap.asDriver().drive(onNext: {[unowned self] in
            self.showPopover(withMessage: PasswordValidator().criterias.map { "* " + $0.error.localizedDescription }.joined(separator: "\n\n"), sourceView: self.passwordInfoButton, arrowDirection: .right)
            
        }).addDisposableTo(disposeBag)
        
        signUpViewModel.signUpTap.do(onNext: {[unowned self] in self.view.endEditing(true)}).map { true }.drive(activityIndicator.rx.isAnimating).addDisposableTo(disposeBag)
        signUpViewModel.didComplete.map { _ in false }.drive(activityIndicator.rx.isAnimating).addDisposableTo(disposeBag)
        
        signUpViewModel.didComplete.drive(onNext: {[weak self] error in
            if let error = error {
                self?.handle(error: error)
            }
        }).addDisposableTo(disposeBag)
    }
}
