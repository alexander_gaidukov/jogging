//
//  String+.swift
//  Jogging
//
//  Created by Alexandr Gaidukov on 30/03/2017.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}

extension String: LocalizedError {
    public var errorDescription: String? {
        return localized
    }
}
