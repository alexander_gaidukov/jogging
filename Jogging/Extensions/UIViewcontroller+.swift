//
//  UIViewcontroller+.swift
//  Jogging
//
//  Created by Alexander Gaidukov on 3/31/17.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import UIKit

extension UIViewController {
    
    class func instance() -> Self {
        return instantiateViewController(withType: self)
    }
    
    private class func instantiateViewController<ViewController: UIViewController>(withType type: ViewController.Type) -> ViewController {
        
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(describing: type)) as! ViewController
        
        return controller
    }
}

extension UIViewController: UIPopoverPresentationControllerDelegate {
    private func popoverDefaultWidth(forSourceRect rect: CGRect , arrowDirection: UIPopoverArrowDirection) -> CGFloat {
        
        let offset: CGFloat = 30.0
        
        let width: CGFloat
        
        switch arrowDirection {
        case UIPopoverArrowDirection.up, UIPopoverArrowDirection.down:
            width = view.bounds.size.width
        case UIPopoverArrowDirection.left:
            width = view.bounds.size.width - (rect.origin.x + rect.size.width)
        case UIPopoverArrowDirection.right:
            width = rect.origin.x
        default:
            fatalError("Unsupported arrow direction")
        }
        
        return width - offset
    }
    
    func showPopover(withMessage message: String, sourceView: UIView, preferdWidth: CGFloat? = nil, arrowDirection: UIPopoverArrowDirection) {
        
        let rect = sourceView.superview!.convert(sourceView.frame, to: self.view)
        
        let width = preferdWidth ?? popoverDefaultWidth(forSourceRect: rect, arrowDirection: arrowDirection)
        
        let controller = PopoverMessageViewController(message: message)
        controller.modalPresentationStyle = .popover
        controller.preferredContentSize = controller.size(forWidth: width)
        
        controller.popoverPresentationController?.permittedArrowDirections = arrowDirection
        controller.popoverPresentationController?.sourceView = sourceView
        controller.popoverPresentationController?.delegate = self
        
        let x: CGFloat
        let y: CGFloat
        
        if arrowDirection == .up {
            x = rect.size.width/2
            y = rect.size.height
        } else if arrowDirection == .down {
            x = rect.size.width/2
            y = 0.0
        } else if arrowDirection == .left {
            x = rect.size.width
            y = rect.size.height / 2
        } else {
            x = 0
            y = rect.size.height / 2
        }
        
        controller.popoverPresentationController?.sourceRect = CGRect(x: x, y: y, width: 0, height: 0)
        
        present(controller, animated: true, completion: nil)
        
    }
    
    public func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}
