//
//  AuthenticationManager.swift
//  Jogging
//
//  Created by Alexandr Gaidukov on 30/03/2017.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import Foundation
import RxSwift

final class AuthenticationManager {
    static let shared = AuthenticationManager()
    
    func authenticate(withEmail email: String, password: String) -> Observable<Error?> {
        let authResource = Resource<String>(path: "users/login", params: ["username": email, "password": password], method: .post, parseJSON: { json in
            
            return (json as? JSON)?["id"] as? String
        })
        
        return WebService.shared.load(resource: authResource).do(onNext: {[weak self] result in
            if let value = result.value {
                self?.saveSessionToken(value)
            }
        }).map {
            $0.error
        }
        
    }
    
    func register(withEmail email: String, password: String, name: String) -> Observable<Error?> {
        let registerResource = Resource<User>(path: "/users", params: ["username": email, "displayname": name, "password": password], method: .post, parseJSON: { json in
            
            return (json as? JSON).flatMap(User.init)
        })
        
        return WebService.shared.load(resource: registerResource).flatMapLatest { [unowned self] result -> Observable<Error?> in
            
            if let error = result.error {
                return Observable.just(error)
            }
            
            return self.authenticate(withEmail: email, password: password)
        }
    }
    
    private func saveSessionToken(_ token: String) {
        print("Token: " + token)
    }
}
