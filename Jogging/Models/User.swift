//
//  User.swift
//  Jogging
//
//  Created by Alexandr Gaidukov on 08/04/2017.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import Foundation

struct User {
    let id: String
    let name: String
    let email: String
}

extension User {
    init?(json: JSON) {
        guard let id = json["id"] as? String else{
            return nil
        }
        
        self.id = id
        name = json["displayname"] as? String ?? ""
        email = json["username"] as? String ?? ""
    }
}
