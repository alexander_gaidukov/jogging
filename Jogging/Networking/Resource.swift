//
//  Resource.swift
//  Jogging
//
//  Created by Alexandr Gaidukov on 06/04/2017.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import Foundation

typealias JSON = [String: Any]
typealias HTTPHeaderFields = [String: String]

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}

struct Resource<A> {
    let url: URL
    let params: JSON
    let method: HTTPMethod
    let headers: HTTPHeaderFields
    let parse: (Data) -> A?
}

extension Resource {
    
    init(path: String, params: JSON = [:], method: HTTPMethod = .get, headers: HTTPHeaderFields = [:], parse: @escaping (Data) -> A?) {
        
        let url = URL(path: path, params: params, method: method)
        
        self.init(url: url, params: params, method: method, headers: headers, parse: parse)
    }
    
    init(path: String, params: JSON = [:], method: HTTPMethod = .get, headers: HTTPHeaderFields = [:], parseJSON: @escaping (Any) -> A?) {
        
        let parse: (Data) -> A? = { data in
            let json = try? JSONSerialization.jsonObject(with: data, options: [])
            return json.flatMap(parseJSON)
        }
        
        self.init(path: path, params: params, method: method, headers: headers, parse: parse)
    }
}
