//
//  Result.swift
//  Jogging
//
//  Created by Alexandr Gaidukov on 06/04/2017.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import Foundation

enum Result<A> {
    case success(A)
    case error(Error)
}

extension Result {
    
    var value: A? {
        guard case let .success(value) = self else {
            return nil
        }
        return value
    }
    
    var error: Error? {
        guard case let .error(error) = self else {
            return nil
        }
        return error
    }
    
    init(value: A?, orError error: Error) {
        if let value = value {
            self = .success(value)
        } else {
            self = .error(error)
        }
    }
}
