//
//  WebService.swift
//  Jogging
//
//  Created by Alexandr Gaidukov on 06/04/2017.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import Foundation
import RxSwift

extension URL {
    init(path: String, params: JSON, method: HTTPMethod) {
        var components = URLComponents()
        components.scheme = Constants.Server.scheme
        components.host = Constants.Server.host
        components.path = path.hasPrefix("/") ? path : "/" + path
        
        switch method {
        case .get, .delete:
            components.queryItems = params.map {
                return URLQueryItem(name: $0.key, value: String(describing: $0.value))
            }
        default:
            break
        }
        
        self = components.url!
    }
}

extension URLRequest {
    init<A>(resource: Resource<A>) {
        
        self.init(url: resource.url)
        
        httpMethod = resource.method.rawValue
        
        var headers = resource.headers
        headers["Accept"] = headers["Accept"] ?? "application/json"
        headers["Content-Type"] = headers["Content-Type"] ?? "application/json"
        
        for header in headers {
            setValue(header.value, forHTTPHeaderField: header.key)
        }
        
        cachePolicy = .reloadIgnoringLocalCacheData
        
        switch resource.method {
        case .post, .put:
            httpBody = try? JSONSerialization.data(withJSONObject: resource.params, options: [])
        default:
            break
        }
    }
}

final class WebService {
    static let shared = WebService()
    private var session = URLSession(configuration: .default)
    
    func clearSession() {
        session = URLSession(configuration: .default)
    }
    
    func load<A>(resource: Resource<A>, completion: @escaping(Result<A>) -> ()) -> URLSessionDataTask? {
        guard Reachability.isConnectedToNetwork() else {
            completion(Result(value: nil, orError: WebServiceError.noInternetConnection))
            return nil
        }
        
        let request = URLRequest(resource: resource)
        
        let task = session.dataTask(with: request) { data, response, _ in
            
            if let response = response as? HTTPURLResponse, (200..<300) ~= response.statusCode {
                let parsed = data.flatMap(resource.parse)
                completion(Result(value: parsed, orError: WebServiceError.other))
            } else {
                let error = data.flatMap(WebServiceError.init) ?? WebServiceError.other
                completion(Result(value: nil, orError: error))
            }
        }
        
        task.resume()
        
        return task
    }
    
    func load<A>(resource: Resource<A>) -> Observable<Result<A>> {
        return Observable.create { observer in
            
            let task = self.load(resource: resource) { result in
                observer.onNext(result)
                observer.onCompleted()
            }
            
            return Disposables.create {
                task?.cancel()
            }
        }
    }
}
