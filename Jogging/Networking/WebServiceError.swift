//
//  WebServiceError.swift
//  Jogging
//
//  Created by Alexandr Gaidukov on 06/04/2017.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import Foundation

enum WebServiceError {
    case noInternetConnection
    case custom(String)
    case other
}

extension WebServiceError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .noInternetConnection:
            return "The Internet connection appears to be offline".localized
        case .other:
            return "Unfortunately something went wrong".localized
        case .custom(let message):
            return message
        }
    }
}

extension WebServiceError {
    
    init?(data: Data) {
        if let object = try? JSONSerialization.jsonObject(with: data, options: []), let json = object as? JSON {
            
            if let message =  json["message"] as? String {
                self = .custom(message)
            } else if let errors = json["errors"] as? [String: String] {
                let message = errors.map {
                    return $0.key + " " + $0.value
                }.joined(separator: ", ")
                self = .custom(message)
            } else {
                self = .other
            }
        } else {
            self = .other
        }
    }
}
