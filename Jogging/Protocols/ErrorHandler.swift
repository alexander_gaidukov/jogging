//
//  ErrorHandler.swift
//  Jogging
//
//  Created by Alexander Gaidukov on 3/31/17.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import UIKit

protocol ErrorHandler {
    func handle(errors: [Error])
}

extension ErrorHandler {
    
    func handle(errors: [Error]) {
        print("!!!Error: \(errors.map{$0.localizedDescription}.joined(separator: ", "))")
    }
    
    func handle(error: Error) {
        handle(errors: [error])
    }
}

extension ErrorHandler where Self: UIViewController {
    func handle(errors: [Error]) {
        let message = errors.map{$0.localizedDescription}.joined(separator: ", ")
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK".localized, style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
}
