//
//  Validator.swift
//  Jogging
//
//  Created by Alexandr Gaidukov on 30/03/2017.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import Foundation

struct Criteria<Item> {
    let error: Error
    let validate: (Item) -> Bool
}

enum ValidationResult {
    case success
    case failure([Error])
}

extension ValidationResult {
    var boolValue: Bool {
        if case .success = self {
            return true
        }
        return false
    }
}

class Validator<Item> {
    
    var criterias: [Criteria<Item>] = []
    
    func validate(_ item: Item, forceStop: Bool = false) -> ValidationResult {
        
        var errors: [Error] = []
        
        for criteria in criterias {
            
            if !criteria.validate(item) {
                
                errors.append(criteria.error)
                
                if forceStop { break }
            }
        }
        
        return errors.isEmpty ? .success : .failure(errors)
    }
}

class EmailValidator: Validator<String> {
    override init() {
        super.init()
        let criteria = Criteria<String>(error: "Wrong email format") {
            let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
            return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: $0)
        }
        criterias = [criteria]
    }
}

class PasswordValidator: Validator<String> {
    override init() {
        super.init()
        
        let sizeCriteria = Criteria<String>(error: "Password length should be in range of 6-40 characters") {
            return (6...40) ~= $0.characters.count
        }
        
        let capitalizedCriteria = Criteria<String>(error: "Password should contain at least 1 uppercased and lowercased letter") {
            return $0.lowercased() != $0 && $0.uppercased() != $0
        }
        
        let digitCriteria = Criteria<String>(error: "Password should contain at least 1 number") {
            $0.rangeOfCharacter(from: CharacterSet.decimalDigits) != nil
        }
        
        criterias = [sizeCriteria, capitalizedCriteria, digitCriteria]
    }
}
