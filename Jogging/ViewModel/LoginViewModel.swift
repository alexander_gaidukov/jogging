//
//  LoginViewModel.swift
//  Jogging
//
//  Created by Alexandr Gaidukov on 30/03/2017.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

final class LoginViewModel {
    
    let email: Driver<String>
    let password: Driver<String>
    let loginDidTap: Driver<Void>
    let signUpDidTap: Driver<Void>
    
    let isLoginButtonEnabled: Driver<Bool>
    let didComplete: Driver<Error?>
    private let userNameAndPassword: Driver<(String, String)>
    
    let disposeBag = DisposeBag()
    
    init(email: Driver<String>, password: Driver<String>, loginDidTap: Driver<Void>, signUpDidTap: Driver<Void>) {
        
        self.email = email
        self.password = password
        self.loginDidTap = loginDidTap
        self.signUpDidTap = signUpDidTap
        
        let isEmailValid = email.map { !$0.isEmpty }
        
        let isPasswordValid = password.map { !$0.isEmpty }

        isLoginButtonEnabled = Driver.combineLatest(isEmailValid, isPasswordValid) {
            return $0 && $1
        }
        
        userNameAndPassword = Driver.combineLatest(email, password) {
            ($0, $1)
        }
        
        didComplete = loginDidTap.withLatestFrom(userNameAndPassword).flatMapLatest { (email, password) in
            AuthenticationManager.shared.authenticate(withEmail: email, password: password).asDriver(onErrorRecover: { error  in
                return Driver.just(error)
            })
        }

        signUpDidTap.drive(onNext: {
            Wireframe.shared.showSignUp()
        }).addDisposableTo(disposeBag)
    }
}
