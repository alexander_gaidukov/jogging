//
//  SignUpViewModel.swift
//  Jogging
//
//  Created by Alexandr Gaidukov on 01/04/2017.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

final class SignUpViewModel {
    
    let email: Driver<String>
    let name: Driver<String>
    let password: Driver<String>
    let passwordConfirmation: Driver<String>
    let signInTap: Driver<Void>
    let signUpTap: Driver<Void>
    
    let emailBorderColor: Driver<UIColor>!
    let passwordBorderColor: Driver<UIColor>!
    let nameBorderColor: Driver<UIColor>!
    let passwordConfirmationBorderColor: Driver<UIColor>!
    
    var credentials: Driver<(String, String, String)>!
    
    let signUpButtonEnable: Driver<Bool>!
    let passwordInfoButtonHidden: Driver<Bool>!
    
    let disposeBag = DisposeBag()
    
    let didComplete: Driver<Error?>
    
    
    
    init(email: Driver<String>, name: Driver<String>, password: Driver<String>, passwordConfirmation: Driver<String>, signInTap: Driver<Void>, signUpTap: Driver<Void>) {
        self.email = email
        self.password = password
        self.passwordConfirmation = passwordConfirmation
        self.name = name
        self.signInTap = signInTap
        self.signUpTap = signUpTap
        
        credentials = Driver.combineLatest(email, name, password) {
            return ($0, $1, $2)
        }
        
        emailBorderColor = email.map { email in
            if email.isEmpty {
                return .clear
            }
            
            return EmailValidator().validate(email, forceStop: true).boolValue ? .green : .red
        }
        
        nameBorderColor = name.map { name in
            return name.isEmpty ? .clear : .green
        }
        
        passwordBorderColor = password.map { password in
            if password.isEmpty {
                return .clear
            }
            
            return PasswordValidator().validate(password, forceStop: true).boolValue ? .green : .red
        }
        
        passwordConfirmationBorderColor = Driver.combineLatest(password, passwordConfirmation) { password, confirmation in
            
            if confirmation.isEmpty {
                return .clear
            }
            
            return password == confirmation ? .green : .red
        }
        
        signUpButtonEnable = Driver.combineLatest(email, name, password, passwordConfirmation) { email, name, password, confirmation in
            
            return EmailValidator().validate(email, forceStop: true).boolValue && PasswordValidator().validate(password, forceStop: true).boolValue && !name.isEmpty && password == confirmation
        }
        
        passwordInfoButtonHidden = password.map { password in
            return password.isEmpty || PasswordValidator().validate(password, forceStop: true).boolValue
        }
        
        didComplete = signUpTap.withLatestFrom(credentials).flatMapLatest{ (email, name, password) in
            AuthenticationManager.shared.register(withEmail: email, password: password, name: name).asDriver(onErrorRecover: { error in
                return Driver.just(error)
            })
        }
        
        signInTap.drive(onNext: {
            Wireframe.shared.dismissSignUp()
        }).addDisposableTo(disposeBag)
    }
}
