//
//  Wireframe.swift
//  Jogging
//
//  Created by Alexander Gaidukov on 3/31/17.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import UIKit

final class Wireframe {
    
    static let shared = Wireframe()
    
    private var window: UIWindow? {
        return (UIApplication.shared.delegate as? AppDelegate)?.window
    }
    
    private var rootViewController: UIViewController? {
        return window?.rootViewController
    }
    
    func showSignUp() {
        let signUpViewController = SignUpViewController.instance()
        signUpViewController.modalTransitionStyle = .flipHorizontal
        rootViewController?.present(signUpViewController, animated: true, completion: nil)
    }
    
    func dismissSignUp() {
        rootViewController?.dismiss(animated: true, completion: nil)
    }
}
