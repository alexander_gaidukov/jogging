//
//  ValidatorTests.swift
//  Jogging
//
//  Created by Alexandr Gaidukov on 30/03/2017.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import XCTest

class ValidatorTests: XCTestCase {
    
    let emailValidator = EmailValidator()
    let passwordValidator = PasswordValidator()
    
    func testEmailsValidation() {
        let validEmails = ["alexander.gaidukov@gmail.com", "company_member@company.com", "company123-member12@gmail.com", "ivan.ivanov_123-78@mail.ru"]
        
        let invalidEmails = ["", "hello.ru", "~123gen@gmail.com", "a.g@mail.r"]
        
        for email in validEmails {
            if case .failure(_) = emailValidator.validate(email) {
                XCTAssertTrue(false, "Email should be valid")
            }
        }
        
        for email in invalidEmails {
            if case .success = emailValidator.validate(email) {
                XCTAssertTrue(false, "Email should not be valid")
            }
        }
    }
    
    func testPasswordsValidation() {
        let validPasswords = ["BeYghjhghg1", "CfUnY1", "hhhh123GHJ3w"]
        
        let invalidPasswords = ["", "dDds1", "qwejjdiejsnnesjnedoode", "assgdr123", "Dsdetpkjltb"]
        
        for password in validPasswords {
            if case .failure(_) = passwordValidator.validate(password) {
                XCTAssertTrue(false, "Password should be valid")
            }
        }
        
        for password in invalidPasswords {
            if case .success = passwordValidator.validate(password) {
                XCTAssertTrue(false, "Password should not be valid")
            }
        }
    }
    
}
